app.factory("fleetsFactory", function($http, $cookies, SERVICE) {
    var urlBase = SERVICE.api + 'fleet';
    var fleetsFactory = {};

    fleetsFactory.getFleets = function() {
        $http.defaults.headers.common.Authorization = 'Bearer ' + $cookies.get('token');
        return $http.get(urlBase);
    };

    fleetsFactory.getFleet = function(id) {
        $http.defaults.headers.common.Authorization = 'Bearer ' + $cookies.get('token');
        return $http.get(urlBase + '/' + id);
    };

    fleetsFactory.insertFleet = function(fleet) {
        $http.defaults.headers.common.Authorization = 'Bearer ' + $cookies.get('token');
        return $http.post(urlBase, fleet);
    };

    fleetsFactory.updateFleet = function(fleet) {
        $http.defaults.headers.common.Authorization = 'Bearer ' + $cookies.get('token');
        return $http.put(urlBase + '/' + fleet.ID, fleet)
    };

    /*
     fleetsFactory.deleteFleet = function(id) {
     $http.defaults.headers.common.Authorization = 'Bearer ' + $cookies.get('token');
     return $http.delete(urlBase + '/' + id);
     };
     */

    fleetsFactory.getFleetPods = function(id) {
        $http.defaults.headers.common.Authorization = 'Bearer ' + $cookies.get('token');
        return $http.get(urlBase + '/' + id + '/pod/');
    };

    fleetsFactory.getFleetPodLocations = function(id) {
        $http.defaults.headers.common.Authorization = 'Bearer ' + $cookies.get('token');
        return $http.get(urlBase + '/' + id + '/pods/locations/');
    };

    return fleetsFactory;
});