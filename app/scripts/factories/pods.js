app.factory("podsFactory", function($http, $cookies, SERVICE) {
  var urlBase = SERVICE.api + 'pod';
  var podsFactory = {};

  podsFactory.getPods = function() {
    $http.defaults.headers.common.Authorization = 'Bearer ' + $cookies.get('token');
    return $http.get(urlBase);
  };

  podsFactory.getPod = function(id) {
    $http.defaults.headers.common.Authorization = 'Bearer ' + $cookies.get('token');
    return $http.get(urlBase + '/' + id);
  };

  podsFactory.insertPod = function(fleet) {
    $http.defaults.headers.common.Authorization = 'Bearer ' + $cookies.get('token');
    return $http.post(urlBase, fleet);
  };

  podsFactory.updatePod = function(fleet) {
    $http.defaults.headers.common.Authorization = 'Bearer ' + $cookies.get('token');
    return $http.put(urlBase + '/' + fleet.ID, fleet)
  };

  podsFactory.deletePod = function(id) {
    $http.defaults.headers.common.Authorization = 'Bearer ' + $cookies.get('token');
    return $http.delete(urlBase + '/' + id);
  };

  return podsFactory;
});