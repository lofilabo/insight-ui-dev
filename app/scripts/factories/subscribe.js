app.factory("subscribeService", ['$http', '$cookies', 'userProvider', 'SERVICE',
    function($http, $cookies, userProvider, SERVICE) {

        var subscribe = {};

        subscribe.getChannelName = function(url, cb) {
            if (!userProvider.isLoggedIn()) {
                return false;
            }
            $http.post(SERVICE.api + url, {}).then(function(res) {
                return cb(null, res.data.channel_name);
            }, function(res) {
                if (res.data.channel_name) {
                    return cb(null, res.data.channel_name);
                }
                return cb(res.status);
            });
        }

        subscribe.getSocket = function(channel_name) {
            
            if (!userProvider.isLoggedIn()) {
                return false;
            }
            var socket = io('http://' + sockets_loopback_IP + ':' + sockets_loopback_PORT + '?channel=' + channel_name +
                '&token=' + $cookies.get('token'));
            return socket;
        }

        subscribe.listen = function(socket, channel_name, event, cb) {
            socket.on(channel_name + ':' + event, function(data) {
                return cb(data);
            });
        }

        return subscribe;
    }
]);