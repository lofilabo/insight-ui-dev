app.factory("usersFactory", function($http, $cookies, SERVICE) {
    var urlBase = SERVICE.api + 'user';
    var usersFactory = {};

    usersFactory.getUsers = function() {
        $http.defaults.headers.common.Authorization = 'Bearer ' + $cookies.get('token');
        return $http.get(urlBase);
    };

    usersFactory.getUser = function(id) {
        $http.defaults.headers.common.Authorization = 'Bearer ' + $cookies.get('token');
        return $http.get(urlBase + '/' + id);
    };

    usersFactory.insertUser = function(user) {
        $http.defaults.headers.common.Authorization = 'Bearer ' + $cookies.get('token');
        return $http.post(urlBase, user);
    };

    usersFactory.updateUser = function(user) {
        $http.defaults.headers.common.Authorization = 'Bearer ' + $cookies.get('token');
        return $http.put(urlBase + '/' + user.ID, user)
    };

    /*
     usersFactory.deleteUser = function(id) {
     $http.defaults.headers.common.Authorization = 'Bearer ' + $cookies.get('token');
     return $http.delete(urlBase + '/' + id);
     };
     */

    usersFactory.userRoles = {
        'FLEET_ADMIN': 'Fleet admin',
        'MEDIA_ADMIN': 'Media admin',
        'SERVICE_USER': 'Service user',
        'UNREGISTERED': 'Unregistered',
        'MAINTENANCE': 'Engineer',
    };

    return usersFactory;
});