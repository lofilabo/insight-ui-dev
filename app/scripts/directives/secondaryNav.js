'use strict';

/**
 * @ngdoc function
 * @name adminApp.controller:FleetsCtrl
 * @description
 * # FleetsCtrl
 * Controller of the adminApp
 */

angular.module('adminApp')
.directive("secondaryNav", function() {
    return {
        templateUrl: "views/templates/secondaryNavigation.html",
        controller: ['$scope', '$cookies', '$state', function($scope, $cookies, $state) {
            $scope.fleetId = $cookies.get('fleetId');

            $scope.goTo = function (where, params)
            {
                $state.go(where, params);
            }

            $scope.getCurrentState = function ()
            {
                return $state.current.name;
            }
        }]
    };
});