'use strict';

/**
 * @ngdoc function
 * @name adminApp.controller:FleetsCtrl
 * @description
 * # FleetsCtrl
 * Controller of the adminApp
 */

angular.module('adminApp')
.directive("mainNav", function() {
    return {
        templateUrl: "views/templates/navigation.html",
        controller: ['$scope', '$cookies', '$state', '$http', 'SERVICE', function($scope, $cookies, $state, $http, SERVICE) {
            $scope.fleetId = $cookies.get('fleetId');

            $scope.logout = function ()
            {
                $http.post(SERVICE.auth + 'logout').then(function(res) {
                    angular.forEach($cookies, function (v, k) {
                        $cookies.remove(k);
                    });
                    $state.go('login');
                });
            }
        }]
    };
});