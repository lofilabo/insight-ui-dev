'use strict';

/**
 * @ngdoc function
 * @name adminApp.controller:NotificationsCtrl
 * @description
 * # NotificationsCtrl
 * Controller of the adminApp
 */
angular.module('adminApp')
    .controller('NotificationsCtrl', ['$scope', '$rootScope', '$http', '$state', 'userProvider', 'SERVICE', '$cookies', '$mdDialog', 'NOTIF_SERVICE',
        function($scope, $rootScope, $http, $state, userProvider, SERVICE, $cookies, $mdDialog, NOTIF_SERVICE) {
            $scope.list = NOTIF_SERVICE.list;
            $scope.meta = NOTIF_SERVICE.meta;


            //$scope.iconState = NOTIF_SERVICE.iconAlert;
            //$scope.viewState = NOTIF_SERVICE.showAlerts;

            $scope.toggle = function() {
                $scope.meta.iconActive = !$scope.meta.iconActive;
                $scope.meta.viewActive = !$scope.meta.viewActive;
            }

            $scope.add = function() {
                $scope.list.push({
                    title: 'Title',
                    message: 'test',
                    datetime: '1/1/2000',
                    severity: 2,
                    issued_to: 'Everyone',
                    status: 'Open'
                });
            }
        }
    ]);