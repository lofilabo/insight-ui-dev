/**
 * @ngdoc function
 * @name adminApp.controller:FleetsCtrl
 * @description
 * # FleetsCtrl
 * Controller of the adminApp
 */
angular.module('adminApp')
    .controller('PodCtrl', ['$scope', '$http', '$state', 'userProvider', 'SERVICE', 'podsFactory', '$stateParams',
        function($scope, $http, $state, userProvider, SERVICE, podsFactory, $stateParams) {
            if (!userProvider.isLoggedIn()) {
                $state.go('home');
            }
            console.log($stateParams.fleetId);
            console.log($stateParams.podId);
            $scope.status;
            $scope.pod;

            getPod();
            getFeatures();

            function getPod() {
                podsFactory.getPod($stateParams.podId)
                    .then(function(response) {
                        console.log(response);
                        $scope.pod = response.data.data;
                    }, function(error) {
                        console.log(error);
                        $scope.status = 'Unable to load Pod data: ' + error.message;
                    });
                $scope.podStates = ['OK', 'CRITICAL', 'WARNING', 'UNKNOWN'];
            }

            function getFeatures() {
                $http.get(SERVICE.api + 'pod/feature').then(function(res) {
                    $scope.features = res.data.data;
                }, function(res) {
                    console.log(res);
                });
            }

            $scope.updatePod = function() {
                $http.put(SERVICE.api + 'pod/' + $scope.pod.id, $scope.pod).then(function(res) {
                    $scope.pod = res.data.data;
                }, function(res) {
                    console.log(res);
                });
            }

            $scope.updateFeatureList = function(featureId, pod) {
                // if selected id is in the list, remove it from package
                if (pod.feature_ids.indexOf(String(featureId)) !== -1) {
                    $http.delete(SERVICE.api + 'pod/' + pod.id + '/feature/' + featureId).then(function(res) {
                        console.log('deleted');
                    }, function(res) {
                        console.log('delete fail');
                    });
                } else {
                    $http.post(SERVICE.api + 'pod/' + pod.id + '/feature/' + featureId, {}).then(function(res) {
                            console.log('OK');
                            console.log(res);
                            pod = res.data.data;
                        },
                        function(res) {
                            console.log('FAIL');
                            console.log(res);
                        });
                }
            }

            $scope.updateState = function(pod) {
                $http.post(SERVICE.api + 'pod/' + pod.id + '/state', pod).then(function(res) {
                        console.log('OK');
                        console.log(res);
                        pod = res.data.data;
                    },
                    function(res) {
                        console.log('FAIL');
                        console.log(res);
                    });
            }

            $scope.reactivatePod = function(pod) {
                $http.post(SERVICE.api + 'pod/' + pod.id + '/reactivate', pod).then(function(res) {
                        console.log('OK');
                        console.log(res);
                        // otherwise pod does not get updated
                        $scope.pod = res.data.data;
                    },
                    function(res) {
                        console.log('FAIL');
                        console.log(res);
                    });
            }

            $scope.deactivatePod = function(pod) {
                $http.post(SERVICE.api + 'pod/' + pod.id + '/deactivate', pod).then(function(res) {
                        console.log('OK');
                        console.log(res);
                        $scope.pod = res.data.data;
                    },
                    function(res) {
                        console.log('FAIL');
                        console.log(res);
                    });
            }

            $scope.updatePod = function() {
                $http.put(SERVICE.api + 'pod/' + $scope.pod.id, $scope.pod).then(function(res) {
                    $scope.pod = res.data.data;
                }, function(res) {
                    console.log(res);
                });
            }

            $scope.deletePod = function() {
                $http.delete(SERVICE.api + 'pod/' + $scope.pod.id).then(function(res) {
                    $state.go('pods');
                }, function(res) {
                    console.log(res);
                });
            }
        }
    ]);