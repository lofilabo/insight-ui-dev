'use strict';

/**
 * @ngdoc function
 * @name adminApp.controller:FleetsCtrl
 * @description
 * # FleetsCtrl
 * Controller of the adminApp
 */
angular.module('adminApp')
    .controller('WaypointCtrl', ['$scope', '$http', '$state', 'userProvider', 'SERVICE', '$cookies', '$stateParams',
        function($scope, $http, $state, userProvider, SERVICE, $cookies, $stateParams) {
            if (!userProvider.isLoggedIn()) {
                $state.go('home');
            }

            init();

            $scope.updateWaypoint = function() {
                $http.put(SERVICE.api + 'waypoint/' + $scope.waypoint.id, $scope.waypoint).then(function(res) {
                    $scope.waypoint = res.data.data;
                }, function(res) {
                    console.log(res);
                });
            }

            $scope.deleteWaypoint = function() {
                $http.delete(SERVICE.api + 'waypoint/' + $scope.waypoint.id).then(function(res) {
                    $state.go('waypoints');
                }, function(res) {
                    console.log(res);
                });
            }

            function init() {
                if (!$stateParams.id) {
                    $state.go('waypoints');
                }
                $scope.waypoint = {};
                $scope.waypointStates = {
                    OK: 'OK',
                    OUT_OF_SERVICE: 'Out of service',
                    WARNING: 'Warning',
                    CRITICAL: 'Critical',
                };
                $http.get(SERVICE.api + 'waypoint/' + $stateParams.id).then(function(res) {
                    $scope.waypoint = res.data.data;
                }, function(res) {
                    console.log(res);
                });
            }

        }
    ]);