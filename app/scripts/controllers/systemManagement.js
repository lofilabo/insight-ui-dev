'use strict';

/**
 * @ngdoc function
 * @name adminApp.controller:systemManagementCtrl
 * @description
 * # systemManagementCtrl
 * Controller of the adminApp
 */
angular.module('adminApp')
    .controller('SystemManagementCtrl', ['$scope', '$rootScope', '$http', '$state', 'userProvider', 'SERVICE', '$cookies', '$mdDialog',
        function($scope, $rootScope, $http, $state, userProvider, SERVICE, $cookies, $mdDialog) {
            if (!userProvider.isLoggedIn()) {
                $state.go('home');
            }
            $scope.currentNavItem = 'system-settings';

        }
    ]);