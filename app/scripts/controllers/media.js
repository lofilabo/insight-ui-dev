'use strict';

/**
 * @ngdoc function
 * @name adminApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the adminApp
 */
angular.module('adminApp')
    .controller('MediaCtrl', ['$scope', '$http', '$state', '$cookies', 'userProvider', 'SERVICE',
        function($scope, $http, $state, $cookies, userProvider, SERVICE) {

            init();

            $scope.addPackage = function(mediaPackage) {
                if (!$scope.newPackageForm.$valid) {
                    alert('Info missing');
                    return;
                }
                $http.post(SERVICE.api + 'media/package', mediaPackage).then(function(res) {
                    $scope.mediaPacks.push(res.data.data);
                    $scope.newPackage = {};
                    $scope.showAddPackage = false;
                }, function(res) {
                    alert('fail');
                });
            }

            function init() {
                $scope.fleetId = $cookies.get('fleetId');
                // fleet id required to load data, redirect to select fleet if id is not present
                if (!$scope.fleetId) {
                    $state.go('fleets');
                }
                if (!userProvider.isLoggedIn()) {
                    $state.go('home');
                }
                $scope.newPackage = {};
                $scope.mediaPacks = [];
                $scope.pods = [];
                $scope.mediaItems = [];
                $http.get(SERVICE.api + 'media/package').then(function(res) {
                    $scope.mediaPacks = res.data.data;
                    $http.get(SERVICE.api + 'pod').then(function(res) {
                        $scope.pods = res.data.data;
                        $http.get(SERVICE.api + 'media/item').then(function(res) {
                            for (var i in res.data.data) {
                                $scope.mediaItems[res.data.data[i].id] = res.data.data[i];
                            }
                        });
                    });
                });
            }

        }
    ]);