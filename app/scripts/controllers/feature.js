'use strict';

/**
 * @ngdoc function
 * @name adminApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the adminApp
 */
angular.module('adminApp')
    .controller('FeatureCtrl', ['$scope', '$http', '$state', 'userProvider', 'SERVICE', '$cookies',
        function($scope, $http, $state, userProvider, SERVICE, $cookies) {
            if (!userProvider.isLoggedIn()) {
                console.log('DENY : Redirecting to home');
                $state.go('home');
            } else {
                console.log('ALLOW');
                $http.defaults.headers.common.Authorization = 'Bearer ' + $cookies.get('token');
            }
            $http.get(SERVICE.api + 'pod/feature').then(function(res) {
                $scope.features = res.data.data;
            }, function(res) {
                console.log(res);
            });


            $scope.addFeature = function(feature) {
                console.log($scope.features);
                $http.post(SERVICE.api + 'pod/feature', feature).then(function(res) {
                    $scope.features.push(res.data.data);
                    $scope.newFeat = {};
                }, function(res) {
                    console.log(res);
                });
                console.log($scope.features);
            }

            $scope.save = function(feature, index) {
                $http.put(SERVICE.api + 'pod/feature/' + feature.id, feature).then(function(res) {
                    $scope.features[index] = res.data.data;
                }, function(res) {
                    console.log(res);
                });
            }

            $scope.delete = function(feature, index) {
                $http.delete(SERVICE.api + 'pod/feature/' + feature.id).then(function(res) {
                    $scope.features.splice(index, 1);
                }, function(res) {
                    console.log(res);
                });
            }

            $scope.cancel = function(feature) {
                feature = {};
            }
        }
    ]);