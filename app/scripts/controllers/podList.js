'use strict';

/**
 * @ngdoc function
 * @name adminApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the adminApp
 */
angular.module('adminApp')
    .controller('PodListCtrl', ['$scope', '$http', '$state', 'userProvider', 'SERVICE', '$cookies',
        function($scope, $http, $state, userProvider, SERVICE, $cookies) {
            init();
            $scope.insertPod = function(pod) {
                if (!$scope.newPodForm.$valid) {
                    alert('Info missing');
                    return;
                }
                $http.post(SERVICE.api + 'fleet/' + $scope.fleetId + '/pod', pod).then(function(res) {
                    $scope.pods.push(res.data.data);
                    $scope.newPod = {};
                    $scope.showAddPod = false;
                }, function(res) {
                    alert('fail');
                });
            }

            function init() {
                $scope.fleetId = $cookies.get('fleetId');
                if (!$scope.fleetId) {
                    $state.go('fleets');
                }
                if (!userProvider.isLoggedIn()) {
                    $state.go('login');
                }
                $http.get(SERVICE.api + 'fleet/' + $scope.fleetId + '/pod').then(function(res) {
                    $scope.pods = res.data.data;
                }, function(res) {
                    console.log(res);
                });

                $http.get(SERVICE.api + 'fleet/' + $scope.fleetId + '/waypoint').then(function(res) {
                    $scope.waypoints = res.data.data;
                }, function(res) {
                    console.log(res);
                });
            }

            $scope.currentPage = 0;
            $scope.pageSize = 1;
        }
    ]);