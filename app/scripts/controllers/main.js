'use strict';

/**
 * @ngdoc function
 * @name adminApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the adminApp
 */
angular.module('adminApp')
    .controller('MainCtrl', ['$scope', '$http', '$state', 'CLIENT', 'SERVICE', 'store', 'userProvider', '$cookies',
        function($scope, $http, $state, CLIENT, SERVICE, store, userProvider, $cookies) {
            $scope.loggedIn = false;
            $scope.email = 'admin@insight.com';
            $scope.password = 'passpass';
            $http.defaults.headers.common.Content_Type = 'application/json';
            $http.defaults.headers.common.Accept = 'application/json';
            $scope.password_token = 'passpass';
            $scope.email_token = '';

            if (userProvider.isLoggedIn()) {
                $scope.loggedIn = true;
                // $http.defaults.headers.common.Authorization = 'Bearer ' + $cookies.get('token');
                $http.get(SERVICE.api + 'user').then(function(res) {
                    if (res.data && !res.data.error) {
                        $scope.users = res.data;
                    }
                });
                loadPage();
            } else {
              //$state.go('login');
            }

            $scope.login = function() {
                $http.post(SERVICE.auth + 'oauth/token', {
                    'grant_type': 'password',
                    'client_id': CLIENT.clientId,
                    'client_secret': CLIENT.clientKey,
                    'password': $scope.password,
                    'username': $scope.email,
                    'scope': '*'
                }).then(function(res) {
                    if (res.data.access_token) {
                        console.log('TOKEN OK');
                        // console.log(res.access_token);
                        $cookies.put('token', res.data.access_token);
                        $cookies.put('refresh_token', res.data.refresh_token);
                        $http.defaults.headers.common.Authorization = 'Bearer ' + res.data.access_token;
                        $scope.loggedIn = true;
                        loadPage();
                    } else {
                        $scope.message = "Details not found on our system. Please check your email and/or password and try again.";
                    }
                }, function(res) {
                    $scope.message = "Details not found on our system. Please check your email and/or password and try again.";
                });
            }

            function loadPage() {
                if ($scope.loggedIn) {
                    $http.get(SERVICE.api + 'fleet').then(function(res) {
                        var fleets = res.data.data;
                        if (fleets.length === 1) {
                            $cookies.put('fleetId', fleets[0].id);
                            $cookies.put('fleetName', fleets[0].name);
                            $state.go('fleet-status');
                        } else {
                            $state.go('fleets');
                        }
                    });
                    // $http.get(SERVICE.api + 'pod').then(function (res) {
                    //     $scope.updates = [];
                    //     $http.post(SERVICE.api + 'stream/all-in-fleet').then(function (res) {
                    //         var channel = res.data.channel_name;
                    //         subscribeToUpdates(channel);
                    //     }, function (res) {
                    //         if(res.status === 409) {
                    //             subscribeToUpdates(res.data.channel_name);
                    //         }
                    //     });
                    //     console.log(res.data);
                    //     $scope.sortedPods = $scope.sortPods(res.data);
                    //     console.log('SORTED');
                    //     console.log($scope.sortedPods);
                    // });
                }
            }

            function subscribeToUpdates(channel) {
                var socket = io('http://' + sockets_loopback_IP + ':' + sockets_loopback_PORT +'?channel=' + channel +
                    '&token=' + $cookies.get('token'));
                socket.on(channel + ':Modules\\Stream\\Events\\UpdateEvent', function(data) {
                    console.log(data);
                    $scope.updates.push(data);
                    $scope.$apply();
                });
            }

            $scope.getToken = function() {
                $http.post(SERVICE.auth + 'oauth/token', {
                    'grant_type': 'password',
                    'client_id': CLIENT.clientId,
                    'client_secret': CLIENT.clientKey,
                    'password': $scope.password_token,
                    'username': $scope.email_token,
                    'scope': '*'
                }).then(function(res) {
                    $scope.user_token = res.data.access_token;
                    $scope.showToken = true;
                });
            }

            $scope.logout = function() {
                $http.post(SERVICE.api + 'logout').then(function(res) {
                    $scope.loggedIn = false;
                    $scope.$apply();
                });
            }
        }
    ]);

angular.module('adminApp')
    .controller('MobileNavigationCtrl', ['$scope', '$mdSidenav',
        function($scope, $mdSidenav) {
            $scope.toggleMobileMenu = function() {
                $mdSidenav('mobile-navigation').toggle()
            };
        }

    ]);
