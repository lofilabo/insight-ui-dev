'use strict';

/**
 * @ngdoc function
 * @name adminApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the adminApp
 */
angular.module('adminApp')
    .controller('ReservationsCtrl', ['$scope', '$http', '$state', '$cookies', 'userProvider', 'SERVICE', 'subscribeService',
        function($scope, $http, $state, $cookies, userProvider, SERVICE, subscribeService) {

            init();

            $scope.cancelReservation = function(resId, index) {
                $http.post(SERVICE.api + 'reservation/' + resId + '/cancel', {}).then(function(res) {
                        $scope.reservations[res.data.data.id] = res.data.data;
                    },
                    function(res) {

                    });
            }

            function receiveUpdate(data) {
                console.log(data);
                var reservation = data.reservation;
                for (var i in $scope.reservations) {
                    if ($scope.reservations[i].id === reservation.id) {
                        // replace old reservation with updated one
                        $scope.reservations[i] = reservation;
                        // exit loop
                        i = $scope.reservations.length + 1;
                        $scope.$apply();
                    }
                }
            }

            function receiveCreate(data) {
                $scope.reservations.unshift(data.reservation);
                $scope.$apply();
            }

            function subscribeToReservationUpdates() {
                subscribeService.getChannelName('stream/reservation/all', function(err, channel_name) {
                    var socket = subscribeService.getSocket(channel_name);
                    subscribeService.listen(socket, channel_name, 'Modules\\Reservation\\Events\\ReservationCreate', receiveCreate);
                    subscribeService.listen(socket, channel_name, 'Modules\\Reservation\\Events\\ReservationUpdate', receiveUpdate);
                });
            }

            function init() {
                $scope.fleetId = $cookies.get('fleetId');
                if (!$scope.fleetId) {
                    $state.go('fleets');
                }
                if (!userProvider.isLoggedIn()) {
                    $state.go('login');
                }
                $http.get(SERVICE.api + 'fleet/' + $scope.fleetId + '/reservation/active').then(function(res) {
                    $scope.reservations = res.data.data;
                }, function(res) {
                    console.log(res);
                });
            }
        }
    ]);
// todo: implement admin middleware, apply admin middleware, create new function to login admins,
// that will check whether user is super admin and call oauth func to get token