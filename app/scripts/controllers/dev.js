'use strict';

/**
 * @ngdoc function
 * @name adminApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the adminApp
 */
angular.module('adminApp')
    .controller('DevCtrl', ['$scope', '$http', '$state', 'userProvider', 'SERVICE', '$cookies',
        function($scope, $http, $state, userProvider, SERVICE, $cookies) {
            // if (!userProvider.isLoggedIn()) {
            //   console.log('DENY : Redirecting to home');
            //   $state.go('home');
            // } else {
            //   console.log('ALLOW');
            // }


            $scope.getRoutes = function() {
                $http.get(SERVICE.api + 'dev/routes').then(function(res) {
                    $scope.routes = res.data.routes;
                });
            }
        }
    ]);