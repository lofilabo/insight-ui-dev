'use strict';

/**
 * @ngdoc function
 * @name adminApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the adminApp
 */
angular.module('adminApp')
    .controller('MediaDetailCtrl', ['$scope', '$http', '$state', '$cookies', 'userProvider', 'SERVICE', '$stateParams',
        function($scope, $http, $state, $cookies, userProvider, SERVICE, $stateParams) {
            init();

            $scope.saveMediaSetting = function(mediaSettings, packageId, itemId) {
                $http.put(SERVICE.api + 'media/settings/' + mediaSettings.id, mediaSettings).then(function(res) {
                        console.log('OK');
                        console.log(res);
                        mediaSettings = res.data.data;
                    },
                    function(res) {
                        console.log('FAIL');
                        console.log(res);
                    });
            }

            $scope.updateMediaList = function(mediaId, mediaPackage) {
                // if selected id is in the list, remove it from package
                if (mediaPackage.media_items.indexOf(String(mediaId)) !== -1) {
                    $http.delete(SERVICE.api + 'media/settings/' + mediaPackage.media_data[mediaId].id).then(function(res) {
                        console.log('deleted');
                    }, function() {
                        console.log('delete fail');
                    });
                } else {
                    $http.post(SERVICE.api + 'media/package/' + mediaPackage.id + '/item/' + mediaId).then(function(res) {
                            console.log('OK');
                            console.log(res);
                            mediaPackage.media_data[mediaId] = res.data.data;
                        },
                        function(res) {
                            console.log('FAIL');
                            console.log(res);
                        });
                }
            }

            $scope.updatePodList = function(podId, mediaPackage) {
                // if selected id is in the list, remove it from package
                if (mediaPackage.pods.indexOf(String(podId)) !== -1) {
                    $http.delete(SERVICE.api + 'pod/' + podId + '/media/package/' + mediaPackage.id).then(function(res) {
                        console.log('deleted');
                    }, function() {
                        console.log('delete fail');
                    });
                } else {
                    $http.post(SERVICE.api + 'pod/' + podId + '/media/package/' + mediaPackage.id).then(function(res) {
                            console.log('OK');
                            console.log(res);
                        },
                        function(res) {
                            console.log('FAIL');
                            console.log(res);
                        });
                }
            }

            $scope.deactivatePackage = function(packId) {
                $http.post(SERVICE.api + 'media/package/deactivate/' + packId).then(function(res) {
                        $scope.mediaPack.active = 0;
                    },
                    function(res) {

                    });
            }

            $scope.activatePackage = function(packId) {
                $http.post(SERVICE.api + 'media/package/activate/' + packId).then(function(res) {
                        $scope.mediaPack.active = 1;
                    },
                    function(res) {

                    });
            }

            $scope.deletePackage = function(mediaPackageId) {
                $http.delete(SERVICE.api + 'media/package/' + mediaPackageId).then(function(res) {
                        $state.go('media');
                    },
                    function(res) {
                        console.log('FAIL');
                        console.log(res);
                    });
            }

            $scope.updatePackage = function(mediaPack) {
                $http.put(SERVICE.api + 'media/package/' + mediaPack.id, mediaPack).then(function(res) {
                        $scope.mediaPack = res.data.data;
                    },
                    function(res) {
                        console.log('FAIL');
                        console.log(res);
                    });
            }

            function init() {
                if (!$stateParams.packageId) {
                    $state.go('media');
                }
                $scope.fleetId = $cookies.get('fleetId');
                if (!userProvider.isLoggedIn()) {
                    $state.go('login');
                }
                // fleet id required to load data, redirect to select fleet if id is not present
                if (!$scope.fleetId) {
                    $state.go('fleets');
                }
                $scope.newPackage = {};
                $scope.mediaPacks = [];
                $scope.pods = [];
                $scope.mediaItems = [];
                $http.get(SERVICE.api + 'media/package/' + $stateParams.packageId).then(function(res) {
                    $scope.mediaPack = res.data.data;
                    $http.get(SERVICE.api + 'fleet/' + $scope.fleetId + '/pod').then(function(res) {
                        $scope.pods = res.data.data;
                        $http.get(SERVICE.api + 'media/item').then(function(res) {
                            for (var i in res.data.data) {
                                $scope.mediaItems[res.data.data[i].id] = res.data.data[i];
                            }
                        });
                    });
                });
            }

        }
    ]);