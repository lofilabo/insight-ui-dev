'use strict';

/**
 * @ngdoc function
 * @name adminApp.controller:LoginCtrl
 * @description
 * # LoginCtrl
 * Controller of the adminApp
 */
angular.module('adminApp')
    .controller('LoginCtrl', ['$scope', '$http', 'CLIENT', 'SERVICE', 'store', 'userProvider', '$cookies', '$mdToast', '$state',
        function($scope, $http, CLIENT, SERVICE, store, userProvider, $cookies, $mdToast, $state) {
            $scope.email = 'admin@insight.com';
            $scope.password = 'passpass';
            $http.defaults.headers.common.Content_Type = 'application/json';
            $http.defaults.headers.common.Accept = 'application/json';
            $scope.password_token = 'passpass';
            $scope.email_token = '';
            $scope.loadingBar = false;
            $scope.errorBar = false;

            $scope.login = function() {
                $scope.loadingBar = true;
                $mdToast.show(
                    $mdToast.simple()
                        .textContent('Evaluating your credentials')
                        .position('top left')
                        .hideDelay(1000)
                );

                $http.post(SERVICE.auth + 'oauth/token', {
                    'grant_type': 'password',
                    'client_id': CLIENT.clientId,
                    'client_secret': CLIENT.clientKey,
                    'password': $scope.password,
                    'username': $scope.email,
                    'scope': '*'
                }).then(function(res) {
                    console.log(res);
                    if (res.data.access_token) {
                        console.log('TOKEN OK');
                        // console.log(res.access_token);
                        $cookies.put('token', res.data.access_token);
                        $cookies.put('refresh_token', res.data.refresh_token);
                        $http.defaults.headers.common.Authorization = 'Bearer ' + res.data.access_token;
                        $scope.loggedIn = true;
                        $scope.loadingBar = false;
                        var fleetId = $cookies.get('fleetId');
                        if (fleetId) {
                            $state.go('fleet-status');
                        } else {
                            $state.go('fleets');
                        }
                    } else {
                        $scope.loadingBar = false;
                        $scope.errorBar = true;
                        $mdToast.show(
                            $mdToast.simple()
                                .textContent('Details not found on our system. Please check your email and/or password and try again.')
                                .position('top left')
                                .hideDelay(6000)
                        );
                        $scope.help = true;
                    }
                }, function(res) {
                    $scope.loadingBar = false;
                    $scope.errorBar = true;
                    $mdToast.show(
                        $mdToast.simple()
                            .textContent('Details not found on our system. Please check your email and/or password and try again.')
                            .position('top left')
                            .hideDelay(6000)
                    );
                    $scope.help = true;
                });
                $scope.loadingBar = false;
            };
        }
    ]);