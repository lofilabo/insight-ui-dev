'use strict';

/**
 * @ngdoc function
 * @name adminApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the adminApp
 */
angular.module('adminApp')
    .controller('UsersCtrl', ['$scope', '$http', '$state', 'userProvider', 'SERVICE', '$cookies',
        function($scope, $http, $state, userProvider, SERVICE, $cookies) {
            if (!userProvider.isLoggedIn()) {
                $state.go('home');
            }
            $scope.newUser = {
                first_name: "Engi",
                last_name: "Neer",
                email: "engi@insight.com",
                password: "passpass"
            };
            $scope.userRoles = {
                'FLEET_ADMIN': 'Fleet admin',
                'MEDIA_ADMIN': 'Media admin',
                'SERVICE_USER': 'Service user',
                'UNREGISTERED': 'Unregistered',
                'MAINTENANCE': 'Engineer',
            };
            $http.defaults.headers.common.Authorization = 'Bearer ' + $cookies.get('token');
            $scope.users = [];
            $scope.email = '';
            $scope.username = '';
            $scope.password = '';

            $scope.users = [];
            $scope.email = '';
            $scope.username = '';
            $scope.password = '';

            $http.get(SERVICE.api + 'user').then(function(res) {
                $scope.users = res.data.data;
            });

            $scope.addUser = function() {
                $http.post(SERVICE.api + 'user', $scope.newUser).then(function(res) {
                    $scope.users.push(res.data.data)
                }, function(res) {
                    console.log(res);
                });
            }

            $scope.saveUser = function(user) {
                $http.put(SERVICE.api + 'user/' + user.id, user).then(function(res) {
                    console.log('USER EDIT OK');
                    console.log(res);
                });
            }

            $scope.deleteUser = function(userId, index) {
                jQuery.ajax({
                    method: 'delete',
                    url: SERVICE.api + 'user/' + userId,
                    success: function(res) {
                        console.log('USER DEL OK');
                        console.log(res);
                        $scope.users.splice(index, 1);
                        $scope.$apply();
                    }
                });
            }
        }
    ]);