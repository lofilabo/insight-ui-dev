/**
 * @ngdoc function
 * @name adminApp.controller:FleetsCtrl
 * @description
 * # FleetsCtrl
 * Controller of the adminApp
 */
angular.module('adminApp')
    .controller('AllJourneysStatsCtrl', ['$scope', '$http', '$state', 'userProvider', 'SERVICE', 'podsFactory', '$stateParams', '$cookies',
        function($scope, $http, $state, userProvider, SERVICE, podsFactory, $stateParams, $cookies) {

            init();

            function init() {
                if (!userProvider.isLoggedIn()) {
                    console.log('DENY : Redirecting to home');
                    $state.go('home');
                } else {
                    console.log('ALLOW');
                    $http.defaults.headers.common.Authorization = 'Bearer ' + $cookies.get('token');
                }

                $scope.stats = {};
                $http.get(SERVICE.api + 'stats/journey').then(function(res) {
                    $scope.stats = res.data.data;
                }, function(res) {
                    console.log(res);
                });
            }
        }
    ]);