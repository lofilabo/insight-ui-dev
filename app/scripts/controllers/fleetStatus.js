'use strict';

/**
 * @ngdoc function
 * @name adminApp.controller:FleetsCtrl
 * @description
 * # FleetsCtrl
 * Controller of the adminApp
 */
angular.module('adminApp')
    .controller('FleetStatusCtrl', ['$scope', '$http', '$state', 'userProvider', 'SERVICE', '$cookies', 'NgMap',
        function($scope, $http, $state, userProvider, SERVICE, $cookies, NgMap) {
            if (!userProvider.isLoggedIn()) {
                $state.go('home');
            }

            init();

            $scope.getNumCriticalIssues = function(objects) {
                var cnt = 0;
                for (var i in objects) {
                    if (objects[i].state === 'CRITICAL') {
                        cnt++;
                    }
                }
                return cnt;
            }

            $scope.getNumJourneyIssues = function(data) {
                var journeys = data.journeys;
                var cnt = 0;
                for (var i in journeys) {
                    var podId = journeys[i].pod_id;
                    var startWpId = journeys[i].start_waypoint_id;
                    var endWpId = journeys[i].end_waypoint_id;
                    console.log([podId, startWpId, endWpId]);
                    if (data.pods[podId].state === 'CRITICAL' ||
                        data.waypoints[startWpId].state === 'CRITICAL' ||
                        data.waypoints[endWpId].state === 'CRITICAL') {
                        cnt++;
                    }
                }
                return cnt;
            }

            function processUpdate(data) {
                console.log(data);
                var itemId = data.object.id;
                var listName = data.object_type + 's';
                $scope.status[listName][itemId] = data.object;
            }

            function init() {
                var fleetId = $cookies.get('fleetId');
                $scope.fleetName = $cookies.get('fleetName');
                // fleet id required to load data, redirect to select fleet if id is not present
                if (!fleetId) {
                    $state.go('fleets');
                }
                $http.get(SERVICE.api + 'fleet/' + fleetId + '/status')
                    .then(function(res) {
                        $scope.status = res.data.data;
                        $scope.showSummary = true;
                        subscribeToUpdates('fleet-' + fleetId);
                    })
                    .catch(function(res) {

                    });
                NgMap.getMap().then(function(map) {
                    console.log(map.getCenter());
                    console.log('markers', map.markers);
                    console.log('shapes', map.shapes);
                });
            }

            function subscribeToUpdates(channel) {
                var socket = io('http://' + sockets_loopback_IP + ':' + sockets_loopback_PORT + '?channel=' + channel +
                    '&token=' + $cookies.get('token'));
                socket.on(channel + ':Modules\\Stream\\Events\\UpdateEvent', function(data) {
                    processUpdate(data);
                    $scope.$apply();
                });
            }

            function loadGraphData() {
                $scope.podLabels = ["Download Sales", "In-Store Sales", "Mail-Order Sales"];
                $scope.podData = [300, 500, 100];
                $scope.waypointLabels = ["Download Sales", "In-Store Sales", "Mail-Order Sales"];
                $scope.waypointData = [300, 500, 100];
                $scope.journeyLabels = ["Download Sales", "In-Store Sales", "Mail-Order Sales"];
                $scope.journeyData = [300, 500, 100];
            }

            loadGraphData();
        }
    ]);