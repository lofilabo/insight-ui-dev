'use strict';

/**
 * @ngdoc function
 * @name adminApp.controller:FleetsCtrl
 * @description
 * # FleetsCtrl
 * Controller of the adminApp
 */
angular.module('adminApp')
    .controller('WaypointsCtrl', ['$scope', '$http', '$state', 'userProvider', 'SERVICE', '$cookies',
        function($scope, $http, $state, userProvider, SERVICE, $cookies) {

            init();

            $scope.addWaypoint = function(wp) {
                if (!$scope.newWpForm.$valid) {
                    alert('Info missing');
                    return;
                }
                $http.post(SERVICE.api + 'fleet/' + $scope.fleetId + '/waypoint', wp).then(function(res) {
                    $scope.waypoints.push(res.data.data);
                    $scope.newWaypoint = {};
                    $scope.showAddWp = false;
                }, function(res) {
                    alert('fail');
                });
            }

            $scope.goTo = function(waypointId) {
                $state.go('waypoint', {
                    id: waypointId
                });
            }

            function init() {
                $scope.fleetId = $cookies.get('fleetId');
                if (!userProvider.isLoggedIn()) {
                    $state.go('login');
                }
                // fleet id required to load data, redirect to select fleet if id is not present
                if (!$scope.fleetId) {
                    $state.go('fleets');
                }
                $scope.newWaypoint = {};
                $scope.waypoints = [];
                $http.get(SERVICE.api + 'waypoint').then(function(res) {
                    $scope.waypoints = res.data.data;
                }, function(res) {
                    console.log(res);
                });
            }

        }
    ]);