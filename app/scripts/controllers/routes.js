'use strict';

/**
 * @ngdoc function
 * @name adminApp.controller:FleetsCtrl
 * @description
 * # FleetsCtrl
 * Controller of the adminApp
 */
angular.module('adminApp')
    .controller('RoutesCtrl', ['$scope', '$http', '$state', 'userProvider', 'SERVICE', '$cookies',
        function($scope, $http, $state, userProvider, SERVICE, $cookies) {
            if (!userProvider.isLoggedIn()) {
                console.log('DENY : Redirecting to home');
                $state.go('home');
            } else {
                console.log('ALLOW');
                $http.defaults.headers.common.Authorization = 'Bearer ' + $cookies.get('token');
            }

            init();

            $scope.addRoute = function() {
                $http.post(SERVICE.api + 'route', $scope.newRoute).then(function(res) {
                    $scope.routes.push(res.data.data);
                }, function(res) {
                    console.log(res);
                });
            }

            $scope.goTo = function(routeId) {
                $state.go('route', {
                    id: routeId
                });
            }

            function init() {
                $scope.newRoute = {};
                $scope.routes = [];
                $http.get(SERVICE.api + 'route').then(function(res) {
                    $scope.routes = res.data.data;
                }, function(res) {
                    console.log(res);
                });
            }

        }
    ]);