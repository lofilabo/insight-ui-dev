/**
 * @ngdoc function
 * @name adminApp.controller:FleetsCtrl
 * @description
 * # FleetsCtrl
 * Controller of the adminApp
 */
angular.module('adminApp')
    .controller('AllPodsStatsCtrl', ['$scope', '$http', '$state', 'userProvider', 'SERVICE', 'podsFactory', '$stateParams', '$cookies',
        function($scope, $http, $state, userProvider, SERVICE, podsFactory, $stateParams, $cookies) {

            init();

            $scope.getDaysInFleet = function(pod) {
                var today = moment(new Date());
                var createdAt = moment(new Date(pod.created_at));
                return today.diff(createdAt, 'days');
            }

            function init() {
                if (!userProvider.isLoggedIn()) {
                    console.log('DENY : Redirecting to home');
                    $state.go('home');
                } else {
                    console.log('ALLOW');
                    $http.defaults.headers.common.Authorization = 'Bearer ' + $cookies.get('token');
                }

                $scope.pods = [];
                $http.get(SERVICE.api + 'stats/pod').then(function(res) {
                    $scope.pods = res.data.data;
                }, function(res) {
                    console.log(res);
                });
            }
        }
    ]);