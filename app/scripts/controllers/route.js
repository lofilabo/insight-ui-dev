'use strict';

/**
 * @ngdoc function
 * @name adminApp.controller:FleetsCtrl
 * @description
 * # FleetsCtrl
 * Controller of the adminApp
 */
angular.module('adminApp')
    .controller('RouteCtrl', ['$scope', '$http', '$state', 'userProvider', 'SERVICE', '$cookies', '$stateParams',
        function($scope, $http, $state, userProvider, SERVICE, $cookies, $stateParams) {
            if (!userProvider.isLoggedIn()) {
                console.log('DENY : Redirecting to home');
                $state.go('home');
            } else {
                console.log('ALLOW');
                $http.defaults.headers.common.Authorization = 'Bearer ' + $cookies.get('token');
            }

            init();

            $scope.updateRoute = function() {
                $http.put(SERVICE.api + 'route/' + $scope.route.id, $scope.route).then(function(res) {
                    $scope.route = res.data.data;
                }, function(res) {
                    console.log(res);
                });
            }

            $scope.deleteRoute = function() {
                $http.delete(SERVICE.api + 'route/' + $scope.route.id).then(function(res) {
                    $state.go('routes');
                }, function(res) {
                    console.log(res);
                });
            }

            function init() {
                if (!$stateParams.id) {
                    $state.go('routes');
                }
                $scope.route = {};
                $scope.routeStates = {
                    OK: 'OK',
                    OUT_OF_SERVICE: 'Out of service',
                    WARNING: 'Warning',
                };
                $http.get(SERVICE.api + 'route/' + $stateParams.id).then(function(res) {
                    $scope.route = res.data.data;
                }, function(res) {
                    console.log(res);
                });
            }

        }
    ]);