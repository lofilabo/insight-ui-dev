'use strict';

/**
 * @ngdoc function
 * @name adminApp.controller:FleetsCtrl
 * @description
 * # FleetsCtrl
 * Controller of the adminApp
 */
angular.module('adminApp')
    .controller('FleetsCtrl', ['$scope', '$http', '$state', 'userProvider', 'SERVICE', 'fleetsFactory', '$cookies',
        function($scope, $http, $state, userProvider, SERVICE, fleetsFactory, $cookies) {
            if (!userProvider.isLoggedIn()) {
                $state.go('home');
            }
            $scope.$state = $state;
            $scope.status;
            $scope.fleets;

            getFleets();

            function getFleets() {
                fleetsFactory.getFleets()
                    .then(function(response) {
                        console.log(response);
                        $scope.fleets = response.data.data;
                    }, function(error) {
                        console.log(error);
                        $scope.status = 'Unable to load customer data: ' + error.message;
                    });

                $http.get(SERVICE.api + 'fleet/status')
                    .then(function(res) {
                        // console.log(res);
                        $scope.fleets = res.data.data;
                    })
                    .catch(function(res) {

                    });
            }

            $scope.insertFleet = function(fleet) {
                if (!$scope.newFleetForm.$valid) {
                    alert('Info missing');
                    return;
                }
                fleetsFactory.insertFleet(fleet)
                    .then(function(response) {
                        console.log(response);
                        $scope.fleet = response.data.data;
                    }, function(error) {
                        console.log(error);
                        $scope.status = 'Unable to load customer data: ' + error.message;
                    });
                getFleets();
            }

            $scope.selectFleet = function(fleet) {
                $cookies.put('fleetId', fleet.id);
                $cookies.put('fleetName', fleet.name);
                console.log('Going to fleet');
                $state.go('fleet-status');
            }

            /*
             $scope.deleteFleet = function(fleetId) {
             fleetsFactory.deleteFleet(fleetId)
             .then(function (response) {
             console.log(response);
             $scope.fleet = response.data.data;
             }, function (error) {
             console.log(error);
             $scope.status = 'Unable to load customer data: ' + error.message;
             });
             getFleets();
             }
             */

            $scope.currentPage = 0;
            $scope.pageSize = 1;
        }
    ]);

/**
 * @ngdoc function
 * @name adminApp.controller:FleetsCtrl
 * @description
 * # FleetsCtrl
 * Controller of the adminApp
 */
angular.module('adminApp')
    .controller('FleetCtrl', ['$scope', '$http', '$state', 'userProvider', 'SERVICE', 'fleetsFactory', '$stateParams',
        function($scope, $http, $state, userProvider, SERVICE, fleetsFactory, $stateParams) {
            $scope.NAV_ADDITIONAL = 'fleet';
            console.log($stateParams.fleetId);
            $scope.status;
            $scope.fleet;
            $scope.pods;

            getFleet();
            getPods();

            function getFleet() {
                fleetsFactory.getFleet($stateParams.fleetId)
                    .then(function(response) {
                        console.log(response);
                        $scope.fleet = response.data.data;
                    }, function(error) {
                        console.log(error);
                        $scope.status = 'Unable to load customer data: ' + error.message;
                    });
            }

            function getPods() {
                fleetsFactory.getFleetPods($stateParams.fleetId)
                    .then(function(response) {
                        console.log(response);
                        $scope.pods = response.data.data;
                    }, function(error) {
                        console.log(error);
                        $scope.status = 'Unable to load customer data: ' + error.message;
                    });
            }

            $scope.insertPod = function(pod) {
                pod.fleet_id = $stateParams.fleetId
                console.log(pod);
            }
        }
    ]);