'use strict';

/**
 * @ngdoc function
 * @name adminApp.controller:MyAccountCtrl
 * @description
 * # MyAccountCtrl
 * Controller of the adminApp
 */
angular.module('adminApp')
    .controller('MyAccountCtrl', ['$scope', '$rootScope', '$http', '$state', 'userProvider', 'SERVICE', '$cookies', '$mdDialog',
        function($scope, $rootScope, $http, $state, userProvider, SERVICE, $cookies, $mdDialog) {
            init();

            function init() {
                if (!userProvider.isLoggedIn()) {
                    $state.go('login');
                }
                $scope.user = $cookies.getObject('user');
                console.log($scope.user);
            }
        }
    ]);