'use strict';

/**
 * @ngdoc function
 * @name adminApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the adminApp
 */
angular.module('adminApp')
    .controller('MaintenanceCtrl', ['$scope', '$rootScope', '$http', '$state', 'userProvider', 'SERVICE', '$cookies', '$mdDialog',
        function($scope, $rootScope, $http, $state, userProvider, SERVICE, $cookies, $mdDialog) {
            if (!userProvider.isLoggedIn()) {
                console.log('DENY : Redirecting to home');
                $state.go('home');
            } else {
                console.log('ALLOW');
                $http.defaults.headers.common.Authorization = 'Bearer ' + $cookies.get('token');
            }
            init();
            // PART
            $scope.addPart = function() {
                $http.post(SERVICE.api + 'maintenance/part', $scope.newPart).then(function(res) {
                    $scope.parts.push(res.data.data);
                    $scope.newPart = {};
                }, function(res) {
                    console.log(res);
                });
            }

            $scope.savePart = function(part, index) {
                $http.put(SERVICE.api + 'maintenance/part/' + part.id, part).then(function(res) {
                    $scope.parts[index] = res.data.data;
                }, function(res) {
                    console.log(res);
                });
            }

            $scope.deletePart = function(part, index) {
                $http.delete(SERVICE.api + 'maintenance/part/' + part.id).then(function(res) {
                    $scope.parts.splice(index, 1);
                }, function(res) {
                    console.log(res);
                });
            }

            // END PART
            // ISSUE
            $scope.addIssue = function() {
                $http.post(SERVICE.api + 'maintenance/issue', $scope.newIssue).then(function(res) {
                    $scope.issues.push(res.data.data);
                    $scope.newIssue = {};
                }, function(res) {
                    console.log(res);
                });
            }

            $scope.saveIssue = function(issue, index) {
                $http.put(SERVICE.api + 'maintenance/issue/' + issue.id, issue).then(function(res) {
                    $scope.issues[index] = res.data.data;
                }, function(res) {
                    console.log(res);
                });
            }

            $scope.deleteIssue = function(issue, index) {
                var confirm = $mdDialog.prompt()
                    .title('Please give reason for deleting the item')
                    .placeholder('Reason')
                    .ariaLabel('Reason')
                    // .targetEvent(ev)
                    .ok('Done')
                    .cancel('Cancel');

                $mdDialog.show(confirm).then(function(result) {
                    issue.delete_reason = result;
                    $http({
                        url: SERVICE.api + 'maintenance/issue/' + issue.id,
                        method: 'DELETE',
                        data: issue,
                        headers: {
                            "Content-Type": "application/json;charset=utf-8"
                        }
                    }).then(function(res) {
                        $scope.issues.splice(index, 1);
                    }, function(res) {
                        console.log(res);
                    });
                });
            }
            // END ISSUE
            // TASK
            $scope.addTask = function() {
                // fix time format
                var task = {};
                angular.copy($scope.newTask, task);
                $http.post(SERVICE.api + 'maintenance/task', $scope.newTask).then(function(res) {
                    $scope.activeTasks.push(res.data.data);
                    $scope.newTask = {};
                }, function(res) {
                    console.log(res);
                });
            }

            $scope.updateTask = function(task) {
                var issue = findIssue(task.issue_id);
                $scope.populateIds(task, issue.object);
                task.object = issue.object;
                task.object_id = issue.object_id;
            }

            $scope.saveTask = function(task, index) {
                $http.put(SERVICE.api + 'maintenance/task/' + task.id, task).then(function(res) {
                    $scope.activeTasks[index] = res.data.data;
                }, function(res) {
                    console.log(res);
                });
            }

            $scope.deleteTask = function(task, index) {
                var confirm = $mdDialog.prompt()
                    .title('Please give reason for cancelling the task')
                    .placeholder('Reason')
                    .ariaLabel('Reason')
                    // .targetEvent(ev)
                    .ok('Done')
                    .cancel('Cancel');

                $mdDialog.show(confirm).then(function(result) {
                    task.cancelled_reason = result;
                    $http.post(SERVICE.api + 'maintenance/task/cancel/' + task.id, task)
                        .then(function(res) {
                            $scope.activeTasks.splice(index, 1);
                        }, function(res) {
                            console.log(res);
                        });
                });

            }

            // END TASK
            // TEAM
            $scope.addTeam = function() {
                $http.post(SERVICE.api + 'maintenance/team', $scope.newTeam).then(function(res) {
                    $scope.teams.push(res.data.data);
                    $scope.newTeam = {};
                }, function(res) {
                    console.log(res);
                });
            }

            $scope.saveTeam = function(team, index) {
                $http.put(SERVICE.api + 'maintenance/team/' + team.id, team).then(function(res) {
                    $scope.team[index] = res.data.data;
                }, function(res) {
                    console.log(res);
                });
            }

            $scope.deleteTeam = function(team, index) {
                $http.delete(SERVICE.api + 'maintenance/team/' + team.id, issue).then(function(res) {
                    $scope.team.splice(index, 1);
                }, function(res) {
                    console.log(res);
                });
            }

            // END TEAM
            $scope.populateIds = function(obj, type) {
                $http.get(SERVICE.api + type.toLowerCase()).then(function(res) {
                    obj.objects = res.data.data;
                }, function(res) {
                    console.log(res);
                });
            }

            $scope.populateIdsIfNull = function(obj, type) {
                if (!obj.objects) {
                    $scope.populateIds(obj, type);
                }
            }

            function findIssue(issueId) {
                for (var i in $scope.issues) {
                    if (issueId == $scope.issues[i].id) {
                        return $scope.issues[i];
                    }
                }
            }

            function init() {
                $scope.issues = [];
                $scope.activeTasks = [];
                $scope.completedTasks = [];
                $scope.cancelledTasks = [];
                $scope.parts = [];
                $scope.teams = [];
                $scope.maitenanceObjects = {
                    POD: 'Pod',
                    ROUTE: 'Route',
                    WAYPOINT: 'Waypoint',
                    HAILPOINT: 'Hailpoint'
                };
                $scope.issueTypes = {
                    CRITICAL: 'Critical',
                    NON_CRITICAL: 'Non critical',
                    ROUTINE_MAINTENANCE: 'Routine maintenance',
                };
                $scope.workTypes = {
                    INSTALL: 'Install',
                    REMOVE: 'Remove',
                    REPLACE: 'Replace',
                    INSPECT: 'Inspect',
                    UPDATE: 'Update',
                };
                $scope.taskStatuses = {
                    SCHEDULED: 'Scheduled',
                    IN_PROGRESS: 'In progress',
                    CANCELLED: 'Cancelled'
                };
                $http.get(SERVICE.api + 'maintenance/issue').then(function(res) {
                    $scope.issues = res.data.data;
                }, function(res) {
                    console.log(res);
                });

                $http.get(SERVICE.api + 'maintenance/task/filter/active').then(function(res) {
                    $scope.activeTasks = res.data.data;
                    for (var i in $scope.activeTasks) {
                        if ($scope.activeTasks[i].scheduled_time) {
                            $scope.activeTasks[i].scheduled_time = new Date($scope.activeTasks[i].scheduled_date + ' ' + $scope.activeTasks[i].scheduled_time);
                        }
                        if ($scope.activeTasks[i].scheduled_date) {
                            $scope.activeTasks[i].scheduled_date = new Date($scope.activeTasks[i].scheduled_date);
                        }
                    }
                }, function(res) {
                    console.log(res);
                });

                $http.get(SERVICE.api + 'maintenance/task/filter/completed').then(function(res) {
                    $scope.completedTasks = res.data.data;
                }, function(res) {
                    console.log(res);
                });

                $http.get(SERVICE.api + 'maintenance/task/filter/cancelled').then(function(res) {
                    $scope.cancelledTasks = res.data.data;
                }, function(res) {
                    console.log(res);
                });


                $http.get(SERVICE.api + 'maintenance/part').then(function(res) {
                    $scope.parts = res.data.data;
                }, function(res) {
                    console.log(res);
                });

                $http.get(SERVICE.api + 'maintenance/team').then(function(res) {
                    $scope.teams = res.data.data;
                }, function(res) {
                    console.log(res);
                });

                $http.get(SERVICE.api + 'user/engineers').then(function(res) {
                    $scope.engineers = res.data.data;
                }, function(res) {
                    console.log(res);
                });
            }

            $rootScope.truncate = function(string, length) {
                if (string.length > length)
                    return string.substring(0, length) + '...';
                else
                    return string;
            };

            $scope.getEngineers = function(teamId) {
                for (var i in $scope.teams) {
                    if ($scope.teams[i].id == teamId) {
                        return $scope.teams[i].engineers;
                    }
                }
            }
        }
    ]);