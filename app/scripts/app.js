'use strict';

/**
 * @ngdoc overview
 * @name adminApp
 * @description
 * # adminApp
 *
 * Main module of the application.
 */

var sockets_loopback_IP = '192.168.46.76';
var sockets_loopback_PORT = '3000';
console.log(sockets_loopback_IP);

var app = angular
    .module('adminApp', [
        'ngAnimate',
        'ngCookies',
        'ngResource',
        'ui.router',
        'ngSanitize',
        'ngMaterial',
        'angular-storage',
        'mdPickers',
        'mdDataTable',
        'ngMap',
        'chart.js'
    ])
    .config(function($stateProvider, $urlRouterProvider, storeProvider, $mdThemingProvider) {
        $urlRouterProvider.otherwise('/');
        $stateProvider
            .state('home', {
                url: '/',
                templateUrl: 'views/main.html',
                controller: 'MainCtrl'
            })
            .state('login', {
                url: '/login',
                templateUrl: 'views/login.html',
                controller: 'LoginCtrl'
            })
            .state('fleet-status', {
                url: '/fleet-status',
                templateUrl: 'views/fleetStatus.html',
                controller: 'FleetStatusCtrl'
            })
            .state('users', {
                url: '/users',
                templateUrl: 'views/users.html',
                controller: 'UsersCtrl'
            })
            .state('fleets', {
                url: '/fleets',
                templateUrl: 'views/fleets.html',
                controller: 'FleetsCtrl'
            })
            .state('fleet', {
                url: '/fleet/:fleetId',
                templateUrl: 'views/fleet.html',
                controller: 'FleetCtrl'
            })
            .state('waypoints', {
                url: '/waypoints',
                templateUrl: 'views/waypoints.html',
                controller: 'WaypointsCtrl'
            })
            .state('waypoint', {
                url: '/waypoint/:id',
                templateUrl: 'views/waypoint.html',
                controller: 'WaypointCtrl'
            })
            .state('pod-list', {
                url: '/pod-list',
                templateUrl: 'views/podList.html',
                controller: 'PodListCtrl'
            })
            .state('pod', {
                // url: '/fleet/:fleetId/pod/:podId',
                url: '/pod/:podId',
                templateUrl: 'views/pod.html',
                controller: 'PodCtrl'
            })
            .state('reservations', {
                url: '/reservations',
                templateUrl: 'views/reservations.html',
                controller: 'ReservationsCtrl'
            })
            .state('dev', {
                url: '/dev',
                templateUrl: 'views/dev.html',
                controller: 'DevCtrl'
            })
            .state('media', {
                url: '/media',
                templateUrl: 'views/mediaList.html',
                controller: 'MediaCtrl'
            })
            .state('media-detail', {
                url: '/media-detail/:packageId',
                templateUrl: 'views/mediaDetail.html',
                controller: 'MediaDetailCtrl'
            })
            .state('maintenance', {
                url: '/maintenance',
                templateUrl: 'views/maintenance.html',
                controller: 'MaintenanceCtrl'
            })
            .state('feature', {
                url: '/feature',
                templateUrl: 'views/feature.html',
                controller: 'FeatureCtrl'
            })
            .state('all-pods-stats', {
                url: '/all-pods-stats',
                templateUrl: 'views/allPodsStats.html',
                controller: 'AllPodsStatsCtrl'
            })
            .state('all-journeys-stats', {
                url: '/all-journeys-stats',
                templateUrl: 'views/allJourneysStats.html',
                controller: 'AllJourneysStatsCtrl'
            })
            .state('system-management', {
                url: '/system-management',
                templateUrl: 'views/systemManagement.html',
                controller: 'SystemManagementCtrl'
            })
            .state('my-account', {
                url: '/my-account',
                templateUrl: 'views/myAccount.html',
                controller: 'MyAccountCtrl'
            });

        storeProvider.setStore('localStorage');

        $mdThemingProvider.definePalette('flatblue', {
            '50': 'e0eafd',
            '100': 'b3cbfa',
            '200': '80a8f7',
            '300': '4d85f4',
            '400': '266af1',
            '500': '0050ef',
            '600': '0049ed',
            '700': '0040eb',
            '800': '0037e8',
            '900': '0027e4',
            'A100': 'ffffff',
            'A200': 'd8dcff',
            'A400': 'a5afff',
            'A700': '8b98ff',
            'contrastDefaultColor': 'light',
            'contrastDarkColors': [
                '50',
                '100',
                '200',
                '300',
                'A100',
                'A200',
                'A400',
                'A700'
            ],
            'contrastLightColors': [
                '400',
                '500',
                '600',
                '700',
                '800',
                '900'
            ]
        });

        $mdThemingProvider.definePalette('flatgreen', {
            '50': 'e0f1e0',
            '100': 'b3dcb3',
            '200': '80c580',
            '300': '4dad4d',
            '400': '269c26',
            '500': '008a00',
            '600': '008200',
            '700': '007700',
            '800': '006d00',
            '900': '005a00',
            'A100': '8bff8b',
            'A200': '58ff58',
            'A400': '25ff25',
            'A700': '0cff0c',
            'contrastDefaultColor': 'light',
            'contrastDarkColors': [
                '50',
                '100',
                '200',
                '300',
                'A100',
                'A200',
                'A400',
                'A700'
            ],
            'contrastLightColors': [
                '400',
                '500',
                '600',
                '700',
                '800',
                '900'
            ]
        });

        $mdThemingProvider.definePalette('flatred', {
            '50': 'fce3e0',
            '100': 'f7b9b3',
            '200': 'f28a80',
            '300': 'ed5b4d',
            '400': 'e93726',
            '500': 'e51400',
            '600': 'e21200',
            '700': 'de0e00',
            '800': 'da0b00',
            '900': 'd30600',
            'A100': 'fffbfb',
            'A200': 'ffc8c8',
            'A400': 'ff9595',
            'A700': 'ff7c7b',
            'contrastDefaultColor': 'light',
            'contrastDarkColors': [
                '50',
                '100',
                '200',
                '300',
                'A100',
                'A200',
                'A400',
                'A700'
            ],
            'contrastLightColors': [
                '400',
                '500',
                '600',
                '700',
                '800',
                '900'
            ]
        });

        $mdThemingProvider.theme('default')
            .primaryPalette('flatblue', {
                'default': '200',
                'hue-1': '100',
                'hue-2': '600',
                'hue-3': 'A100'
            })
            .accentPalette('flatgreen', {
                'default': '200',
                'hue-1': '100',
                'hue-2': '600',
                'hue-3': 'A100'
            })
            .warnPalette('flatred', {
                'default': '200',
                'hue-1': '100',
                'hue-2': '600',
                'hue-3': 'A100'
            });
    })
    .constant('CLIENT', {
        'clientKey': 'KGdtdYrELCVPgyJ5lOVkW92jBsmi1obKMGKiYBIE',
        clientId: 1
    })
    .constant('SERVICE', {
        api: 'http://insight-api-dev.com/v1/',
        stream: '',
        auth: 'http://insight-api-dev.com/'
    })
    .factory('userProvider', ['$cookies', '$http', 'SERVICE', function($cookies, $http, SERVICE) {
        return {
            isLoggedIn: function() {
                var token = $cookies.get('token');
                if (token) {
                    $http.defaults.headers.common.Authorization = 'Bearer ' + $cookies.get('token');
                    var user = $cookies.getObject('user');
                    if (!user) {
                        $http.get(SERVICE.api + 'user/self').then(function(res) {
                            $cookies.putObject('user', res.data.data);
                            return true;
                        });
                    } else {
                        return true;
                    }
                } else {
                    return false;
                }
            }
        };
    }])
    .factory('unauthInterceptor', ['$location', function($location) {
        var unauthInterceptor = {
            responseError: function(res) {
                if (res.status === 401) {
                    $location.path('/login');
                }
                return res;
            }
        };

        return unauthInterceptor;
    }]).config(['$httpProvider', function($httpProvider) {
        $httpProvider.interceptors.push('unauthInterceptor');
    }]);
