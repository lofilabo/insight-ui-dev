app.service('NOTIF_SERVICE', function() {
    this.meta = {
        iconActive: false,
        viewActive: false
    };
    this.list = [];

    this.addNotification = function(data) {
        this.list.push(data);
    }
});